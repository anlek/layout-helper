# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0] - 2016-01-31
### Fixed
- rails dependency now set to >= 4
- rspec tests that were failing
- rspec deprication warnings

## [0.0.1] - 2014-06-24
### Added
- Initial Release